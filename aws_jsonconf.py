#!/usr/bin/env python
"""
AWSJsonConfig:  The missing boto configuration class.

Written by Sivan Greenberg, March 2013.
"""

import json
import os
import pprint
import sys

class AWSJsonConfig(object):
	"""
	A small wrapper class around Python's
	'json' module to allow easier
	save/retrieve cycle in AWS boto api format, e.g:
	[('aws:autoscaling:launchconfiguration','Ec2KeyName', 'mykeypair'),...]
	read more: 
		http://boto.readthedocs.org/en/latest/ref/beanstalk.html#module-boto.beanstalk
	

	Also supports app-wide configuration via an init.json namespace.
	"""
	options = []
	json = None
	init = None
	fd = None
	file_list = None
	argv = None

	def __init__(self, config_path, argv=None):
		"""
		default behavior: config_path is a config dir.
		otherwise, use named arugment 'file_name' to
		specify just one file to load config from.
		"""
		# save a copy of argv in the object
		# encapsulation in case we need it.
		self.argv = argv
		self.load(config_path+"/init.json")
		self._create_conf_file_list(config_path.rstrip("/"), argv)
		self.load_path()



	def load(self, file_name):
		self._ensure_file(file_name)
		self.json = json.load(self.fd)
		# special case our non amazon init
		# namespace!
		if os.path.basename(file_name)=='init.json':
			self.init = self.json
		else:
			self._parse_options(self.json)
		self.fd.close()



	def _create_conf_file_list(self, pathname, argv=None):
		"""
		if argv not specified, configuration will
		ignore dynamic command line arguments.
		"""
		if not os.path.exists(pathname):
			raise Exception('Configuration path does not exist. Aborting.')
		import glob
		self.file_list = glob.glob(pathname+"/"+"*.json")
		# kept for debugging purposes:
		# print "file_list:"
		# pprint.PrettyPrinter(indent=4).pprint(self.file_list)
		missing = []
		if argv:
			cmd_line_opt_arguments = self.init['CmdLineOptArguments']
			for arg in cmd_line_opt_arguments:
				# don't forget to ignore the
				# executed file name AND the pathname
				# from sys.argv
				if not arg in argv[2:]:
					missing.append(arg)
					self.file_list.remove(
						pathname+"/"+cmd_line_opt_arguments[arg])
				if set(cmd_line_opt_arguments.keys()).issubset(set(missing)):
					errmsg= "!> At least one dyanmic argument is requierd:"+", ".join(cmd_line_opt_arguments.keys())
					raise Exception('!> %s' % errmsg)



	def load_path(self):
		for fn in self.file_list:
			print "--> Loading: %s" % fn
			self.load(fn)



	def save(self, file_name=None, create=False):
		self._ensure_file(file_name, create)
		json.dump(self.json, self.fd)
		self.fd.close()



	def _parse_options(self, json):
		"""
		Iterate over the namespaces and append accordignly to the
		options list, in the tuple form
		"""
		for ns in json:
			for param in json[ns]:
				self.options.append((ns, param, json[ns][param]))



	def _ensure_file(self, file_name, create=False):
		if os.path.exists(file_name):
			self.fd = file(file_name)
		else:
			if create:
				self.fd = file(file_name, 'w')
			else:
				raise Exception('error opening file: %s' % file_name)
		



if __name__ == "__main__":
	"""self unit test"""
	config = AWSJsonConfig(sys.argv[1], sys.argv)
	import bs_utils
	bs_utils.json_print(config.json)
	print '========================'
	bs_utils.json_print(config.init)
	print '========================'
	pprint.PrettyPrinter(indent=4).pprint(config.options)

	
	
