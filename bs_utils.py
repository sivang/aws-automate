#!/usr/bin/env python

import json
import sys

def json_print(data):
	print json.dumps(data, indent=2)


if __name__ == '__main__':
	print sys.argv[1:]
