#!/usr/bin/env python

import logging
logging.basicConfig(filename="boto.log", level=logging.DEBUG)

# required imports
import boto
import os
import sys
import json

# choose a region to connect to
region_info = boto.regioninfo.RegionInfo(
    None, 'us-west-1', 'elasticbeanstalk.us-west-1.amazonaws.com')

# create a client
beanstalk = boto.connect_beanstalk(region=region_info)

#describe the environment so we can fetch balancer name and other stuff
res = beanstalk.describe_environment_resources(environment_name=sys.argv[1])


print "DESCRIBE RESOURCES:"
print json.dumps(res, sort_keys=True, indent=2)
print "\n\n\n\n"



env_resources = res['DescribeEnvironmentResourcesResponse']['DescribeEnvironmentResourcesResult']['EnvironmentResources']
load_balancers = env_resources['LoadBalancers']

print json.dumps(env_resources, indent=2)

print "\n\n"
print load_balancers
