import sys

# this helps us import stuff before os package manager installation.
sys.path.append('./../')

# Path for storing stuff created during the tests
TESTS_OUTPUT_PATH = "./"

